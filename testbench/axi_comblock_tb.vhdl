library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity axi_comblock_tb is
end entity axi_comblock_tb;

architecture Simulation of axi_comblock_tb is
   signal clk, rst, rstn             : std_logic:='1';
   --
   signal reg0,  reg1,  reg2,  reg3  : std_logic_vector(31 downto 0);
   signal reg4,  reg5,  reg6,  reg7  : std_logic_vector(31 downto 0);
   signal reg8,  reg9,  reg10, reg11 : std_logic_vector(31 downto 0);
   signal reg12, reg13, reg14, reg15 : std_logic_vector(31 downto 0);
   --
   signal fifo_empty, fifo_avail     : std_logic;
   signal fifo_data                  : std_logic_vector(15 downto 0);
   signal fifo_start, fifo_done      : std_logic:='0';
   --
   signal stop               : boolean:=FALSE;
   --
   signal axil_regs_awaddr   : std_logic_vector(6 downto 0);
   signal axil_regs_awprot   : std_logic_vector(2 downto 0);
   signal axil_regs_awvalid  : std_logic;
   signal axil_regs_awready  : std_logic;
   signal axil_regs_wdata    : std_logic_vector(31 downto 0);
   signal axil_regs_wstrb    : std_logic_vector((32/8)-1 downto 0);
   signal axil_regs_wvalid   : std_logic;
   signal axil_regs_wready   : std_logic;
   signal axil_regs_bresp    : std_logic_vector(1 downto 0);
   signal axil_regs_bvalid   : std_logic;
   signal axil_regs_bready   : std_logic;
   signal axil_regs_araddr   : std_logic_vector(6 downto 0);
   signal axil_regs_arprot   : std_logic_vector(2 downto 0);
   signal axil_regs_arvalid  : std_logic;
   signal axil_regs_arready  : std_logic;
   signal axil_regs_rdata    : std_logic_vector(31 downto 0);
   signal axil_regs_rresp    : std_logic_vector(1 downto 0);
   signal axil_regs_rvalid   : std_logic;
   signal axil_regs_rready   : std_logic;
      -- Ports of Axi Slave Bus Interface AXIF_DRAM
   signal axif_dram_awid     : std_logic_vector(0 downto 0);
   signal axif_dram_awaddr   : std_logic_vector(17 downto 0);
   signal axif_dram_awlen    : std_logic_vector(7 downto 0);
   signal axif_dram_awsize   : std_logic_vector(2 downto 0);
   signal axif_dram_awburst  : std_logic_vector(1 downto 0);
   signal axif_dram_awlock   : std_logic;
   signal axif_dram_awcache  : std_logic_vector(3 downto 0);
   signal axif_dram_awprot   : std_logic_vector(2 downto 0);
   signal axif_dram_awqos    : std_logic_vector(3 downto 0);
   signal axif_dram_awregion : std_logic_vector(3 downto 0);
   signal axif_dram_awuser   : std_logic_vector(-1 downto 0);
   signal axif_dram_awvalid  : std_logic;
   signal axif_dram_awready  : std_logic;
   signal axif_dram_wdata    : std_logic_vector(31 downto 0);
   signal axif_dram_wstrb    : std_logic_vector((32/8)-1 downto 0);
   signal axif_dram_wlast    : std_logic;
   signal axif_dram_wuser    : std_logic_vector(-1 downto 0);
   signal axif_dram_wvalid   : std_logic;
   signal axif_dram_wready   : std_logic;
   signal axif_dram_bid      : std_logic_vector(0 downto 0);
   signal axif_dram_bresp    : std_logic_vector(1 downto 0);
   signal axif_dram_buser    : std_logic_vector(-1 downto 0);
   signal axif_dram_bvalid   : std_logic;
   signal axif_dram_bready   : std_logic;
   signal axif_dram_arid     : std_logic_vector(0 downto 0);
   signal axif_dram_araddr   : std_logic_vector(17 downto 0);
   signal axif_dram_arlen    : std_logic_vector(7 downto 0);
   signal axif_dram_arsize   : std_logic_vector(2 downto 0);
   signal axif_dram_arburst  : std_logic_vector(1 downto 0);
   signal axif_dram_arlock   : std_logic;
   signal axif_dram_arcache  : std_logic_vector(3 downto 0);
   signal axif_dram_arprot   : std_logic_vector(2 downto 0);
   signal axif_dram_arqos    : std_logic_vector(3 downto 0);
   signal axif_dram_arregion : std_logic_vector(3 downto 0);
   signal axif_dram_aruser   : std_logic_vector(-1 downto 0);
   signal axif_dram_arvalid  : std_logic;
   signal axif_dram_arready  : std_logic;
   signal axif_dram_rid      : std_logic_vector(0 downto 0);
   signal axif_dram_rdata    : std_logic_vector(31 downto 0);
   signal axif_dram_rresp    : std_logic_vector(1 downto 0);
   signal axif_dram_rlast    : std_logic;
   signal axif_dram_ruser    : std_logic_vector(-1 downto 0);
   signal axif_dram_rvalid   : std_logic;
   signal axif_dram_rready   : std_logic;
      -- Ports of Axi Slave Bus Interface AXIF_FIFO
   signal axif_fifo_awid     : std_logic_vector(0 downto 0);
   signal axif_fifo_awaddr   : std_logic_vector(31 downto 0);
   signal axif_fifo_awlen    : std_logic_vector(7 downto 0);
   signal axif_fifo_awsize   : std_logic_vector(2 downto 0);
   signal axif_fifo_awburst  : std_logic_vector(1 downto 0);
   signal axif_fifo_awlock   : std_logic;
   signal axif_fifo_awcache  : std_logic_vector(3 downto 0);
   signal axif_fifo_awprot   : std_logic_vector(2 downto 0);
   signal axif_fifo_awqos    : std_logic_vector(3 downto 0);
   signal axif_fifo_awregion : std_logic_vector(3 downto 0);
   signal axif_fifo_awuser   : std_logic_vector(-1 downto 0);
   signal axif_fifo_awvalid  : std_logic;
   signal axif_fifo_awready  : std_logic;
   signal axif_fifo_wdata    : std_logic_vector(31 downto 0);
   signal axif_fifo_wstrb    : std_logic_vector((32/8)-1 downto 0);
   signal axif_fifo_wlast    : std_logic;
   signal axif_fifo_wuser    : std_logic_vector(-1 downto 0);
   signal axif_fifo_wvalid   : std_logic;
   signal axif_fifo_wready   : std_logic;
   signal axif_fifo_bid      : std_logic_vector(0 downto 0);
   signal axif_fifo_bresp    : std_logic_vector(1 downto 0);
   signal axif_fifo_buser    : std_logic_vector(-1 downto 0);
   signal axif_fifo_bvalid   : std_logic;
   signal axif_fifo_bready   : std_logic;
   signal axif_fifo_arid     : std_logic_vector(0 downto 0);
   signal axif_fifo_araddr   : std_logic_vector(31 downto 0);
   signal axif_fifo_arlen    : std_logic_vector(7 downto 0);
   signal axif_fifo_arsize   : std_logic_vector(2 downto 0);
   signal axif_fifo_arburst  : std_logic_vector(1 downto 0);
   signal axif_fifo_arlock   : std_logic;
   signal axif_fifo_arcache  : std_logic_vector(3 downto 0);
   signal axif_fifo_arprot   : std_logic_vector(2 downto 0);
   signal axif_fifo_arqos    : std_logic_vector(3 downto 0);
   signal axif_fifo_arregion : std_logic_vector(3 downto 0);
   signal axif_fifo_aruser   : std_logic_vector(-1 downto 0);
   signal axif_fifo_arvalid  : std_logic;
   signal axif_fifo_arready  : std_logic;
   signal axif_fifo_rid      : std_logic_vector(0 downto 0);
   signal axif_fifo_rdata    : std_logic_vector(31 downto 0);
   signal axif_fifo_rresp    : std_logic_vector(1 downto 0);
   signal axif_fifo_rlast    : std_logic;
   signal axif_fifo_ruser    : std_logic_vector(-1 downto 0);
   signal axif_fifo_rvalid   : std_logic;
   signal axif_fifo_rready   : std_logic;

begin

   fifo_master_inst: entity work.AXIF_master
   generic map (
      C_M_TARGET_SLAVE_BASE_ADDR => x"00000000",
      C_M_AXI_BURST_LEN          => 1,
      C_M_AXI_ID_WIDTH           => 1,
      C_M_AXI_ADDR_WIDTH         => 32,
      C_M_AXI_DATA_WIDTH         => 32,
      C_M_AXI_AWUSER_WIDTH       => 0,
      C_M_AXI_ARUSER_WIDTH       => 0,
      C_M_AXI_WUSER_WIDTH        => 0,
      C_M_AXI_RUSER_WIDTH        => 0,
      C_M_AXI_BUSER_WIDTH        => 0
   )
   port map (
      INIT_AXI_TXN  => fifo_start,
      TXN_DONE      => fifo_done,
      ERROR         => open,
      --
      M_AXI_ACLK    => clk,
      M_AXI_ARESETN => rstn,
      M_AXI_AWID    => axif_fifo_awid,
      M_AXI_AWADDR  => axif_fifo_awaddr,
      M_AXI_AWLEN   => axif_fifo_awlen,
      M_AXI_AWSIZE  => axif_fifo_awsize,
      M_AXI_AWBURST => axif_fifo_awburst,
      M_AXI_AWLOCK  => axif_fifo_awlock,
      M_AXI_AWCACHE => axif_fifo_awcache,
      M_AXI_AWPROT  => axif_fifo_awprot,
      M_AXI_AWQOS   => axif_fifo_awqos,
      M_AXI_AWUSER  => axif_fifo_awuser,
      M_AXI_AWVALID => axif_fifo_awvalid,
      M_AXI_AWREADY => axif_fifo_awready,
      M_AXI_WDATA   => axif_fifo_wdata,
      M_AXI_WSTRB   => axif_fifo_wstrb,
      M_AXI_WLAST   => axif_fifo_wlast,
      M_AXI_WUSER   => axif_fifo_wuser,
      M_AXI_WVALID  => axif_fifo_wvalid,
      M_AXI_WREADY  => axif_fifo_wready,
      M_AXI_BID     => axif_fifo_bid,
      M_AXI_BRESP   => axif_fifo_bresp,
      M_AXI_BUSER   => axif_fifo_buser,
      M_AXI_BVALID  => axif_fifo_bvalid,
      M_AXI_BREADY  => axif_fifo_bready,
      M_AXI_ARID    => axif_fifo_arid,
      M_AXI_ARADDR  => axif_fifo_araddr,
      M_AXI_ARLEN   => axif_fifo_arlen,
      M_AXI_ARSIZE  => axif_fifo_arsize,
      M_AXI_ARBURST => axif_fifo_arburst,
      M_AXI_ARLOCK  => axif_fifo_arlock,
      M_AXI_ARCACHE => axif_fifo_arcache,
      M_AXI_ARPROT  => axif_fifo_arprot,
      M_AXI_ARQOS   => axif_fifo_arqos,
      M_AXI_ARUSER  => axif_fifo_aruser,
      M_AXI_ARVALID => axif_fifo_arvalid,
      M_AXI_ARREADY => axif_fifo_arready,
      M_AXI_RID     => axif_fifo_rid,
      M_AXI_RDATA   => axif_fifo_rdata,
      M_AXI_RRESP   => axif_fifo_rresp,
      M_AXI_RLAST   => axif_fifo_rlast,
      M_AXI_RUSER   => axif_fifo_ruser,
      M_AXI_RVALID  => axif_fifo_rvalid,
      M_AXI_RREADY  => axif_fifo_rready
   );

   comblock_inst : entity work.comblock_v1_0
   generic map (
      --
      C_ENABLE_DRAM               => TRUE,
      C_ENABLE_FIFO_FPGA_TO_PROC  => TRUE,
      C_ENABLE_FIFO_PROC_TO_FPGA  => TRUE,
      C_REGS_DATA_WIDTH           => 32,
      C_DRAM_DATA_WIDTH           => 32,
      C_DRAM_ADDR_WIDTH           => 16,
      C_DRAM_DEPTH                => 0,
      C_FIFO_DATA_WIDTH           => 16,
      C_FIFO_DEPTH                => 1024,
      C_FIFO_AFULLOFFSET          => 1,
      C_FIFO_AEMPTYOFFSET         => 1
   )
   port map (
      -- Users to add ports here
      -- regs
      reg0_i           => reg0,
      reg1_i           => reg1,
      reg2_i           => reg2,
      reg3_i           => reg3,
      reg4_i           => reg4,
      reg5_i           => reg5,
      reg6_i           => reg6,
      reg7_i           => reg7,
      reg8_i           => reg8,
      reg9_i           => reg9,
      reg10_i          => reg10,
      reg11_i          => reg11,
      reg12_i          => reg12,
      reg13_i          => reg13,
      reg14_i          => reg14,
      reg15_i          => reg15,
      --
      reg0_o           => reg0,
      reg1_o           => reg1,
      reg2_o           => reg2,
      reg3_o           => reg3,
      reg4_o           => reg4,
      reg5_o           => reg5,
      reg6_o           => reg6,
      reg7_o           => reg7,
      reg8_o           => reg8,
      reg9_o           => reg9,
      reg10_o          => reg10,
      reg11_o          => reg11,
      reg12_o          => reg12,
      reg13_o          => reg13,
      reg14_o          => reg14,
      reg15_o          => reg15,
      -- true dual ram
      ram_clk_i        => '0',
      ram_we_i         => '0',
      ram_addr_i       => x"0000",
      ram_data_i       => x"00000000",
      ram_data_o       => open,
      -- fifo
      fifo_clk_i       => clk,
      fifo_clear_i     => rst,
      fifo_we_i        => fifo_avail,
      fifo_data_i      => fifo_data,
      fifo_full_o      => open,
      fifo_afull_o     => open,
      fifo_overflow_o  => open,
      fifo_re_i        => fifo_avail,
      fifo_data_o      => fifo_data,
      fifo_empty_o     => fifo_empty,
      fifo_aempty_o    => open,
      fifo_underflow_o => open,
      -- Ports of Axi Slave Bus Interface AXIL_REGS
      axil_regs_ACLK     => clk,
      axil_regs_ARESETN  => rstn,
      axil_regs_AWADDR   => axil_regs_awaddr,
      axil_regs_AWPROT   => axil_regs_awprot,
      axil_regs_AWVALID  => axil_regs_awvalid,
      axil_regs_AWREADY  => axil_regs_awready,
      axil_regs_WDATA    => axil_regs_wdata,
      axil_regs_WSTRB    => axil_regs_wstrb,
      axil_regs_WVALID   => axil_regs_wvalid,
      axil_regs_WREADY   => axil_regs_wready,
      axil_regs_BRESP    => axil_regs_bresp,
      axil_regs_BVALID   => axil_regs_bvalid,
      axil_regs_BREADY   => axil_regs_bready,
      axil_regs_ARADDR   => axil_regs_araddr,
      axil_regs_ARPROT   => axil_regs_arprot,
      axil_regs_ARVALID  => axil_regs_arvalid,
      axil_regs_ARREADY  => axil_regs_arready,
      axil_regs_RDATA    => axil_regs_rdata,
      axil_regs_RRESP    => axil_regs_rresp,
      axil_regs_RVALID   => axil_regs_rvalid,
      axil_regs_RREADY   => axil_regs_rready,
      -- Ports of Axi Slave Bus Interface AXIF_DRAM
      axif_dram_ACLK     => clk,
      axif_dram_ARESETN  => rstn,
      axif_dram_AWID     => axif_dram_awid,
      axif_dram_AWADDR   => axif_dram_awaddr,
      axif_dram_AWLEN    => axif_dram_awlen,
      axif_dram_AWSIZE   => axif_dram_awsize,
      axif_dram_AWBURST  => axif_dram_awburst,
      axif_dram_AWLOCK   => axif_dram_awlock,
      axif_dram_AWCACHE  => axif_dram_awcache,
      axif_dram_AWPROT   => axif_dram_awprot,
      axif_dram_AWQOS    => axif_dram_awqos,
      axif_dram_AWREGION => axif_dram_awregion,
      axif_dram_AWUSER   => axif_dram_awuser,
      axif_dram_AWVALID  => axif_dram_awvalid,
      axif_dram_AWREADY  => axif_dram_awready,
      axif_dram_WDATA    => axif_dram_wdata,
      axif_dram_WSTRB    => axif_dram_wstrb,
      axif_dram_WLAST    => axif_dram_wlast,
      axif_dram_WUSER    => axif_dram_wuser,
      axif_dram_WVALID   => axif_dram_wvalid,
      axif_dram_WREADY   => axif_dram_wready,
      axif_dram_BID      => axif_dram_bid,
      axif_dram_BRESP    => axif_dram_bresp,
      axif_dram_BUSER    => axif_dram_buser,
      axif_dram_BVALID   => axif_dram_bvalid,
      axif_dram_BREADY   => axif_dram_bready,
      axif_dram_ARID     => axif_dram_arid,
      axif_dram_ARADDR   => axif_dram_araddr,
      axif_dram_ARLEN    => axif_dram_arlen,
      axif_dram_ARSIZE   => axif_dram_arsize,
      axif_dram_ARBURST  => axif_dram_arburst,
      axif_dram_ARLOCK   => axif_dram_arlock,
      axif_dram_ARCACHE  => axif_dram_arcache,
      axif_dram_ARPROT   => axif_dram_arprot,
      axif_dram_ARQOS    => axif_dram_arqos,
      axif_dram_ARREGION => axif_dram_arregion,
      axif_dram_ARUSER   => axif_dram_aruser,
      axif_dram_ARVALID  => axif_dram_arvalid,
      axif_dram_ARREADY  => axif_dram_arready,
      axif_dram_RID      => axif_dram_rid,
      axif_dram_RDATA    => axif_dram_rdata,
      axif_dram_RRESP    => axif_dram_rresp,
      axif_dram_RLAST    => axif_dram_rlast,
      axif_dram_RUSER    => axif_dram_ruser,
      axif_dram_RVALID   => axif_dram_rvalid,
      axif_dram_RREADY   => axif_dram_rready,
      -- Ports of Axi Slave Bus Interface AXIF_FIFO
      axif_fifo_ACLK     => clk,
      axif_fifo_ARESETN  => rstn,
      axif_fifo_AWID     => axif_fifo_awid,
      axif_fifo_AWADDR   => axif_fifo_awaddr(2 downto 0),
      axif_fifo_AWLEN    => axif_fifo_awlen,
      axif_fifo_AWSIZE   => axif_fifo_awsize,
      axif_fifo_AWBURST  => axif_fifo_awburst,
      axif_fifo_AWLOCK   => axif_fifo_awlock,
      axif_fifo_AWCACHE  => axif_fifo_awcache,
      axif_fifo_AWPROT   => axif_fifo_awprot,
      axif_fifo_AWQOS    => axif_fifo_awqos,
      axif_fifo_AWREGION => axif_fifo_awregion,
      axif_fifo_AWUSER   => axif_fifo_awuser,
      axif_fifo_AWVALID  => axif_fifo_awvalid,
      axif_fifo_AWREADY  => axif_fifo_awready,
      axif_fifo_WDATA    => axif_fifo_wdata,
      axif_fifo_WSTRB    => axif_fifo_wstrb,
      axif_fifo_WLAST    => axif_fifo_wlast,
      axif_fifo_WUSER    => axif_fifo_wuser,
      axif_fifo_WVALID   => axif_fifo_wvalid,
      axif_fifo_WREADY   => axif_fifo_wready,
      axif_fifo_BID      => axif_fifo_bid,
      axif_fifo_BRESP    => axif_fifo_bresp,
      axif_fifo_BUSER    => axif_fifo_buser,
      axif_fifo_BVALID   => axif_fifo_bvalid,
      axif_fifo_BREADY   => axif_fifo_bready,
      axif_fifo_ARID     => axif_fifo_arid,
      axif_fifo_ARADDR   => axif_fifo_araddr(2 downto 0),
      axif_fifo_ARLEN    => axif_fifo_arlen,
      axif_fifo_ARSIZE   => axif_fifo_arsize,
      axif_fifo_ARBURST  => axif_fifo_arburst,
      axif_fifo_ARLOCK   => axif_fifo_arlock,
      axif_fifo_ARCACHE  => axif_fifo_arcache,
      axif_fifo_ARPROT   => axif_fifo_arprot,
      axif_fifo_ARQOS    => axif_fifo_arqos,
      axif_fifo_ARREGION => axif_fifo_arregion,
      axif_fifo_ARUSER   => axif_fifo_aruser,
      axif_fifo_ARVALID  => axif_fifo_arvalid,
      axif_fifo_ARREADY  => axif_fifo_arready,
      axif_fifo_RID      => axif_fifo_rid,
      axif_fifo_RDATA    => axif_fifo_rdata,
      axif_fifo_RRESP    => axif_fifo_rresp,
      axif_fifo_RLAST    => axif_fifo_rlast,
      axif_fifo_RUSER    => axif_fifo_ruser,
      axif_fifo_RVALID   => axif_fifo_rvalid,
      axif_fifo_RREADY   => axif_fifo_rready
   );

   fifo_avail <= not fifo_empty;

   do_clock: process
   begin
      while not stop loop
         wait for 1 ps;
         clk <= not clk;
      end loop;
      wait;
   end process do_clock;
   rst  <= '1', '0' after 4 ps;
   rstn <= not rst;

   fifo_test : process
   begin
      wait until rising_edge(clk) and rst='0';
      fifo_start <= '1';
      wait until rising_edge(clk);
      fifo_start <= '0';
      wait for 10 ns;--until falling_edge(fifo_done);
      wait until rising_edge(clk);
      stop <= TRUE;
      wait;
   end process;

end architecture Simulation;
