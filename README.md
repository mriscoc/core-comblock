![ICTP - Core Comblock - INTI](images/header.png)

The Communication Block IP Core (Core ComBlock) is the result of a collaboration between the [Multidisciplinary LABoratory (MLAB)](https://www.ictp.it/research/ap/research/mlab.aspx) from [The Abdus Salam International Centre for Theoretical Physics (ICTP, Italy)](https://www.ictp.it/) and the [Micro and Nanotechnology Center](http://www.inti.gob.ar/microynanoelectronica/cmnb.htm) from the [National Institute of Industrial Technology (INTI, Argentina)](http://www.inti.gob.ar/). It is licensed under the BSD 3-clause. See [LICENSE](LICENSE) for details.

## Motivation

MLAB projects are characterized by solving the high-speed processing in the FPGA and send the resulting data to a PC. The Processor included in devices such as Zynq is mainly considered as a provider of data storage (DDR memory) and Ethernet connections. The ComBlock was created to provide known interfaces (registers, RAM and FIFO) to a user of the Programmable Logic (PL), avoiding the complexity of the system bus provided by the Processor System (PS), which is AXI in case of the Zynq-7000.

A first version was made in Vivado using IPs from Xilinx. To obtain a portable and highly configurable IP, a second version was developed, using VHDL 93 and memories of the [FPGALIB](https://github.com/INTI-CMNB-FPGA/fpga_lib) project (an asynchronous FIFO and a True Dual Port RAM, both of them tested with Xilinx, Intel/Altera and Microsemi devices). The block provides:
* 16 input and 16 output registers (configurable up to 32 bits).
* A True Dual Port RAM, which provides a Simple RAM interface available to the user. Its inclusion, the data width, the address width and the memory depth can be configured.
* Two asynchronous FIFOs, one from PL to PS and another from PS to PL, with indication of empty/full, almost empty/full and underflow/overflow conditions. Their individual inclusion, the data width and the memory depth can be configured.

An AXI Lite interface was used for the registers and AXI Full interfaces for the RAM and FIFOs.

![ComBlock in Vivado](images/vivado/full.png)

*ComBlock Component in Vivado*

![ComBlock Configurations in Vivado](images/vivado/config.png)

*ComBlock Configurations in Vivado*
