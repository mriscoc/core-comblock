/*
* AXI ComBlock hardware test
*
* Author(s):
* * Rodrigo A. Melo
*
* Copyright (c) 2018-2019 Authors, INTI (CMNB) and ICTP (MLAB)
* Distributed under the BSD 3-Clause License
*
* Description: the firmware for a hardware test of the AXI ComBlock
*/

#include <stdio.h>
#include "comblock.h"

#define TOOL vivado // vivado

#if TOOL == vivado
    #include "xparameters.h"
    #define COMBLOCK_REGS_BASE XPAR_COMBLOCK_0_AXIL_REGS_BASEADDR
    #define COMBLOCK_DRAM_BASE XPAR_COMBLOCK_0_AXIF_DRAM_BASEADDR
    #define COMBLOCK_FIFO_BASE XPAR_COMBLOCK_0_AXIF_FIFO_BASEADDR
#endif

#define REGS_DEPTH 16
#define DRAM_DEPTH 16*1024
#define FIFO_DEPTH 1024

int tx_buf[DRAM_DEPTH], rx_buf[DRAM_DEPTH];

int main() {
    int i, val;

    printf("### Testing the ComBlock\r\n");

    printf("* Testing Regs...\r\n");
    printf("Writing and reading from REG0 to REG15 (loopback)\r\n");
    for (i=0; i < REGS_DEPTH; i++) {
        ComBlock_Write(COMBLOCK_REGS_BASE+i*4,i);
    }
    for (i=0; i < REGS_DEPTH; i++) {
        val = ComBlock_Read(COMBLOCK_REGS_BASE+i*4);
        if ( val != REGS_DEPTH-1-i )
           printf("ERROR:  -> Reg %d = %d\r\n",i,val);
    }
    //
    printf("Reading from REG16 to REG31 (to read the output registers values)\r\n");
    for (i=0; i < REGS_DEPTH; i++) {
        val = ComBlock_Read(COMBLOCK_REGS_BASE+16*4+i*4);
        if ( val != i )
           printf("ERROR:  -> Reg %d = %d\r\n",i,val);
    }
    //
    printf("Writing and reading from REG16 to REG31 (alternative to use the output registers)\r\n");
    for (i=0; i < REGS_DEPTH; i++) {
        ComBlock_Write(COMBLOCK_REGS_BASE+16*4+i*4,i*2);
    }
    for (i=0; i < REGS_DEPTH; i++) {
        val = ComBlock_Read(COMBLOCK_REGS_BASE+16*4+i*4);
        if ( val != i*2 )
           printf("ERROR:  -> Reg %d = %d\r\n",i,val);
    }

    printf("* Testing RAM...\r\n");
    for (i=0; i < DRAM_DEPTH; i++) tx_buf[i] = i;
    ComBlock_WriteBulk(COMBLOCK_DRAM_BASE, tx_buf, DRAM_DEPTH);
    ComBlock_ReadBulk(rx_buf, COMBLOCK_DRAM_BASE, DRAM_DEPTH);
    for (i=0; i < DRAM_DEPTH; i++)
        if (tx_buf[i] != rx_buf[i])
           printf("ERROR: Mem[%d] = %d\r\n", i, rx_buf[i]);

    printf("* Testing FIFO...\r\n");
    for (i=0; i < FIFO_DEPTH; i++) {
        ComBlock_Write(COMBLOCK_FIFO_BASE,i);
    }
    for (i=0; i < FIFO_DEPTH; i++) {
        val = ComBlock_Read(COMBLOCK_FIFO_BASE);
        if ( val != i )
           printf("ERROR:  -> FIFO %d = %d\r\n",i,val);
    }

    printf("### Finished\r\n");

    return 0;
}
