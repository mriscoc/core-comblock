#ifndef COMBLOCK_H
#define COMBLOCK_H

#include <string.h>

#define COMBLOCK_IREG0  0
#define COMBLOCK_IREG1  1*4
#define COMBLOCK_IREG2  2*4
#define COMBLOCK_IREG3  3*4
#define COMBLOCK_IREG4  4*4
#define COMBLOCK_IREG5  5*4
#define COMBLOCK_IREG6  6*4
#define COMBLOCK_IREG7  7*4
#define COMBLOCK_IREG8  8*4
#define COMBLOCK_IREG9  9*4
#define COMBLOCK_IREG10 10*4
#define COMBLOCK_IREG11 11*4
#define COMBLOCK_IREG12 12*4
#define COMBLOCK_IREG13 13*4
#define COMBLOCK_IREG14 14*4
#define COMBLOCK_IREG15 15*4

#define COMBLOCK_OREG0  16*4
#define COMBLOCK_OREG1  17*4
#define COMBLOCK_OREG2  18*4
#define COMBLOCK_OREG3  19*4
#define COMBLOCK_OREG4  20*4
#define COMBLOCK_OREG5  21*4
#define COMBLOCK_OREG6  22*4
#define COMBLOCK_OREG7  23*4
#define COMBLOCK_OREG8  24*4
#define COMBLOCK_OREG9  25*4
#define COMBLOCK_OREG10 26*4
#define COMBLOCK_OREG11 27*4
#define COMBLOCK_OREG12 28*4
#define COMBLOCK_OREG13 29*4
#define COMBLOCK_OREG14 30*4
#define COMBLOCK_OREG15 31*4

typedef __UINTPTR_TYPE__ UINTPTR;
typedef __UINT32_TYPE__  u32;

static inline void ComBlock_Write(UINTPTR Addr, u32 Value) {
   volatile u32 *LocalAddr = (volatile u32 *)Addr;
   *LocalAddr = Value;
}

static inline u32 ComBlock_Read(UINTPTR Addr) {
   return *(volatile u32 *) Addr;
}

static inline void ComBlock_WriteBulk(UINTPTR Addr, int *Buffer, u32 Depth) {
   memcpy((UINTPTR *)Addr, Buffer, Depth * sizeof(int));
}

static inline void ComBlock_ReadBulk(int *Buffer, UINTPTR Addr, u32 Depth) {
   memcpy(Buffer, (UINTPTR *)Addr, Depth * sizeof(int));
}

#endif // COMBLOCK_H
