--
-- ComBlock
--
-- Author(s):
-- * Rodrigo A. Melo
--
-- Copyright (c) 2018-2019 Authors, INTI (CMNB) and ICTP (MLAB)
-- Distributed under the BSD 3-Clause License
--
-- Description: A simple and portable COMmunication BLOCK with well know
-- interfaces in the FPGA side
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library FPGALIB;
use FPGALIB.mems.all;

entity comblock is
   generic (
      REGS_IN_ENA        : boolean:=TRUE;
      REGS_IN_DWIDTH     : integer:=32;
      REGS_IN_DEPTH      : integer:=16;
      --
      REGS_OUT_ENA       : boolean:=TRUE;
      REGS_OUT_DWIDTH    : integer:=32;
      REGS_OUT_DEPTH     : integer:=16;
      --
      DRAM_IO_ENA        : boolean:=TRUE;
      DRAM_IO_DWIDTH     : integer:=32;
      DRAM_IO_AWIDTH     : integer:=16;
      DRAM_IO_DEPTH      : integer:=0;
      --
      FIFO_IN_ENA        : boolean:=TRUE;
      FIFO_IN_DWIDTH     : integer:=16;
      FIFO_IN_DEPTH      : integer:=1024;
      FIFO_IN_AFOFFSET   : integer:=1;
      FIFO_IN_AEOFFSET   : integer:=1;
      --
      FIFO_OUT_ENA       : boolean:=TRUE;
      FIFO_OUT_DWIDTH    : integer:=16;
      FIFO_OUT_DEPTH     : integer:=1024;
      FIFO_OUT_AFOFFSET  : integer:=1;
      FIFO_OUT_AEOFFSET  : integer:=1
   );
   port (
      -- regs side a
      a_reg0_i           :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg1_i           :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg2_i           :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg3_i           :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg4_i           :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg5_i           :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg6_i           :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg7_i           :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg8_i           :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg9_i           :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg10_i          :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg11_i          :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg12_i          :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg13_i          :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg14_i          :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg15_i          :  in std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      a_reg0_o           : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg1_o           : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg2_o           : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg3_o           : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg4_o           : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg5_o           : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg6_o           : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg7_o           : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg8_o           : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg9_o           : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg10_o          : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg11_o          : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg12_o          : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg13_o          : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg14_o          : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      a_reg15_o          : out std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      -- regs side b
      b_reg_clk_i        :  in std_logic;
      b_reg_wr_ena_i     :  in std_logic;
      b_reg_wr_val_i     :  in std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
      b_reg_wr_adr_i     :  in std_logic_vector(4 downto 0);
      b_reg_rd_val_o     : out std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
      b_reg_rd_adr_i     :  in std_logic_vector(4 downto 0);
      -- true dual ram side a
      a_ram_clk_i        :  in std_logic;
      a_ram_we_i         :  in std_logic;
      a_ram_addr_i       :  in std_logic_vector(DRAM_IO_AWIDTH-1 downto 0);
      a_ram_data_i       :  in std_logic_vector(DRAM_IO_DWIDTH-1 downto 0);
      a_ram_data_o       : out std_logic_vector(DRAM_IO_DWIDTH-1 downto 0);
      -- true dual ram side b
      b_ram_clk_i        :  in std_logic;
      b_ram_we_i         :  in std_logic;
      b_ram_addr_i       :  in std_logic_vector(DRAM_IO_AWIDTH-1 downto 0);
      b_ram_data_i       :  in std_logic_vector(DRAM_IO_DWIDTH-1 downto 0);
      b_ram_data_o       : out std_logic_vector(DRAM_IO_DWIDTH-1 downto 0);
      -- fifo1 side a
      a_fifo1_clk_i      :  in std_logic;
      a_fifo1_clear_i    :  in std_logic;
      a_fifo1_we_i       :  in std_logic;
      a_fifo1_data_i     :  in std_logic_vector(FIFO_IN_DWIDTH-1 downto 0);
      a_fifo1_full_o     : out std_logic;
      a_fifo1_afull_o    : out std_logic;
      a_fifo1_overflow_o : out std_logic;
      -- fifo1 side b
      b_fifo1_clk_i      :  in std_logic;
      b_fifo1_clear_i    :  in std_logic;
      b_fifo1_re_i       :  in std_logic;
      b_fifo1_data_o     : out std_logic_vector(FIFO_IN_DWIDTH-1 downto 0);
      -- fifo2 side a
      a_fifo2_clk_i      :  in std_logic;
      a_fifo2_clear_i    :  in std_logic;
      a_fifo2_re_i       :  in std_logic;
      a_fifo2_data_o     : out std_logic_vector(FIFO_OUT_DWIDTH-1 downto 0);
      a_fifo2_empty_o    : out std_logic;
      a_fifo2_aempty_o   : out std_logic;
      a_fifo2_underflow_o: out std_logic;
      -- fifo2 side b
      b_fifo2_clk_i      :  in std_logic;
      b_fifo2_clear_i    :  in std_logic;
      b_fifo2_we_i       :  in std_logic;
      b_fifo2_data_i     :  in std_logic_vector(FIFO_OUT_DWIDTH-1 downto 0);
      -- fifo shared status
      b_fifo_stat_o      : out std_logic_vector(5 downto 0)
   );
end comblock;

architecture Structural of comblock is

   type regs_in_type is array (integer range <>) of std_logic_vector(REGS_IN_DWIDTH-1 downto 0);
   signal regs_in  : regs_in_type(0 to 31);

   type regs_out_type is array (integer range <>) of std_logic_vector(REGS_OUT_DWIDTH-1 downto 0);
   signal regs_out : regs_out_type(0 to 15);

begin

   -- -------------------------------------------------------------------------
   -- Registers
   -- -------------------------------------------------------------------------

   reg_in0_g  : if REGS_IN_ENA and REGS_IN_DEPTH >= 1  generate regs_in(0)  <= a_reg0_i;  end generate;
   reg_in1_g  : if REGS_IN_ENA and REGS_IN_DEPTH >= 2  generate regs_in(1)  <= a_reg1_i;  end generate;
   reg_in2_g  : if REGS_IN_ENA and REGS_IN_DEPTH >= 3  generate regs_in(2)  <= a_reg2_i;  end generate;
   reg_in3_g  : if REGS_IN_ENA and REGS_IN_DEPTH >= 4  generate regs_in(3)  <= a_reg3_i;  end generate;
   reg_in4_g  : if REGS_IN_ENA and REGS_IN_DEPTH >= 5  generate regs_in(4)  <= a_reg4_i;  end generate;
   reg_in5_g  : if REGS_IN_ENA and REGS_IN_DEPTH >= 6  generate regs_in(5)  <= a_reg5_i;  end generate;
   reg_in6_g  : if REGS_IN_ENA and REGS_IN_DEPTH >= 7  generate regs_in(6)  <= a_reg6_i;  end generate;
   reg_in7_g  : if REGS_IN_ENA and REGS_IN_DEPTH >= 8  generate regs_in(7)  <= a_reg7_i;  end generate;
   reg_in8_g  : if REGS_IN_ENA and REGS_IN_DEPTH >= 9  generate regs_in(8)  <= a_reg8_i;  end generate;
   reg_in9_g  : if REGS_IN_ENA and REGS_IN_DEPTH >= 10 generate regs_in(9)  <= a_reg9_i;  end generate;
   reg_in10_g : if REGS_IN_ENA and REGS_IN_DEPTH >= 11 generate regs_in(10) <= a_reg10_i; end generate;
   reg_in11_g : if REGS_IN_ENA and REGS_IN_DEPTH >= 12 generate regs_in(11) <= a_reg11_i; end generate;
   reg_in12_g : if REGS_IN_ENA and REGS_IN_DEPTH >= 13 generate regs_in(12) <= a_reg12_i; end generate;
   reg_in13_g : if REGS_IN_ENA and REGS_IN_DEPTH >= 14 generate regs_in(13) <= a_reg13_i; end generate;
   reg_in14_g : if REGS_IN_ENA and REGS_IN_DEPTH >= 15 generate regs_in(14) <= a_reg14_i; end generate;
   reg_in15_g : if REGS_IN_ENA and REGS_IN_DEPTH >= 16 generate regs_in(15) <= a_reg15_i; end generate;

   regs_out_in_g : if REGS_OUT_ENA generate
      regs_out_for_g : for i in 0 to REGS_OUT_DEPTH-1 generate
         regs_in(i+16) <= regs_out(i);
      end generate;
   end generate;

   b_reg_rd_val_o <= regs_in(to_integer(unsigned(b_reg_rd_adr_i)));

   -- -------------------------------------------------------------------------

   process(b_reg_clk_i)
   begin
      if rising_edge(b_reg_clk_i) then
         if b_reg_wr_ena_i = '1' then
            regs_out(to_integer(unsigned(b_reg_wr_adr_i(3 downto 0)))) <= b_reg_wr_val_i;
         end if;
      end if;
   end process;

   reg_out0_g  : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 1  generate a_reg0_o  <= regs_out(0);  end generate;
   reg_out1_g  : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 2  generate a_reg1_o  <= regs_out(1);  end generate;
   reg_out2_g  : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 3  generate a_reg2_o  <= regs_out(2);  end generate;
   reg_out3_g  : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 4  generate a_reg3_o  <= regs_out(3);  end generate;
   reg_out4_g  : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 5  generate a_reg4_o  <= regs_out(4);  end generate;
   reg_out5_g  : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 6  generate a_reg5_o  <= regs_out(5);  end generate;
   reg_out6_g  : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 7  generate a_reg6_o  <= regs_out(6);  end generate;
   reg_out7_g  : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 8  generate a_reg7_o  <= regs_out(7);  end generate;
   reg_out8_g  : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 9  generate a_reg8_o  <= regs_out(8);  end generate;
   reg_out9_g  : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 10 generate a_reg9_o  <= regs_out(9);  end generate;
   reg_out10_g : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 11 generate a_reg10_o <= regs_out(10); end generate;
   reg_out11_g : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 12 generate a_reg11_o <= regs_out(11); end generate;
   reg_out12_g : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 13 generate a_reg12_o <= regs_out(12); end generate;
   reg_out13_g : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 14 generate a_reg13_o <= regs_out(13); end generate;
   reg_out14_g : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 15 generate a_reg14_o <= regs_out(14); end generate;
   reg_out15_g : if REGS_OUT_ENA and REGS_OUT_DEPTH >= 16 generate a_reg15_o <= regs_out(15); end generate;

   -- -------------------------------------------------------------------------
   -- True Dual Port RAM
   -- -------------------------------------------------------------------------

   dram_g : if DRAM_IO_ENA generate
      truedualram_i: TrueDualPortRAM
      generic map (
         AWIDTH   => DRAM_IO_AWIDTH,
         DWIDTH   => DRAM_IO_DWIDTH,
         DEPTH    => DRAM_IO_DEPTH
      )
      port map (
         clk1_i   => a_ram_clk_i,
         clk2_i   => b_ram_clk_i,
         wen1_i   => a_ram_we_i,
         wen2_i   => b_ram_we_i,
         addr1_i  => a_ram_addr_i,
         addr2_i  => b_ram_addr_i,
         data1_i  => a_ram_data_i,
         data2_i  => b_ram_data_i,
         data1_o  => a_ram_data_o,
         data2_o  => b_ram_data_o
      );
   end generate;

   -- -------------------------------------------------------------------------
   -- FIFOs
   -- -------------------------------------------------------------------------

   fifo_ftp_g : if FIFO_IN_ENA generate
      fifo1_i: FIFO
      generic map (
         DWIDTH       => FIFO_IN_DWIDTH,
         DEPTH        => FIFO_IN_DEPTH,
         AEMPTYOFFSET => FIFO_IN_AEOFFSET,
         AFULLOFFSET  => FIFO_IN_AFOFFSET
      )
      port map (
         -- write side
         wr_clk_i     => a_fifo1_clk_i,
         wr_rst_i     => a_fifo1_clear_i,
         wr_en_i      => a_fifo1_we_i,
         data_i       => a_fifo1_data_i,
         full_o       => a_fifo1_full_o,
         afull_o      => a_fifo1_afull_o,
         overflow_o   => a_fifo1_overflow_o,
         -- read side
         rd_clk_i     => b_fifo1_clk_i,
         rd_rst_i     => b_fifo1_clear_i,
         rd_en_i      => b_fifo1_re_i,
         data_o       => b_fifo1_data_o,
         empty_o      => b_fifo_stat_o(0),
         aempty_o     => b_fifo_stat_o(1),
         underflow_o  => b_fifo_stat_o(2),
         valid_o      => open
      );
   end generate;

   fifo_p2g_g : if FIFO_OUT_ENA generate
      fifo2_i: FIFO
      generic map (
         DWIDTH       => FIFO_OUT_DWIDTH,
         DEPTH        => FIFO_OUT_DEPTH,
         AEMPTYOFFSET => FIFO_OUT_AEOFFSET,
         AFULLOFFSET  => FIFO_OUT_AFOFFSET
      )
      port map (
         -- write side
         wr_clk_i     => b_fifo2_clk_i,
         wr_rst_i     => b_fifo2_clear_i,
         wr_en_i      => b_fifo2_we_i,
         data_i       => b_fifo2_data_i,
         full_o       => b_fifo_stat_o(3),
         afull_o      => b_fifo_stat_o(4),
         overflow_o   => b_fifo_stat_o(5),
         -- read side
         rd_clk_i     => a_fifo2_clk_i,
         rd_rst_i     => a_fifo2_clear_i,
         rd_en_i      => a_fifo2_re_i,
         data_o       => a_fifo2_data_o,
         empty_o      => a_fifo2_empty_o,
         aempty_o     => a_fifo2_aempty_o,
         underflow_o  => a_fifo2_underflow_o,
         valid_o      => open
      );
   end generate;

end Structural;